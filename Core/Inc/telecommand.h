/*
 *
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INC_TELECOMMAND_H_
#define INC_TELECOMMAND_H_

#include <stdint.h>
#include "osdlp_queue_handle.h"
#include "watchdog.h"

/**
 * Receive telemetry. Provides a telecommand in case there is one for the
 * specified VCID.
 * @param pkt the buffer where the TC will be stored. The caller must ensure
 * there is enough space in the buffer to hold the telecommand.
 * @param length a pointer to the length variable where the length of the
 * frame will be stored
 * @param cmd_id a pointer to the variable where the command id will be
 * stored
 * @param vcid the VCID
 * @return 0 if a frame was available, negative otherwise
 */
int
receive_tc(uint8_t *pkt, uint16_t *length, uint8_t *cmd_id, uint8_t vcid);

#endif /* INC_TELECOMMAND_H_ */
