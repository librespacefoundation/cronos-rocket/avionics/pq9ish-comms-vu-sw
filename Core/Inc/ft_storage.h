/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FT_STORAGE_H
#define FT_STORAGE_H

#include <stdlib.h>
#include <stdint.h>

/**
 * @brief Software based fault tolerant storage
 *
 * This header files provides a fault-tolerant implementation for data storage
 * in the flash memory
 */


struct ft_storage_priv {
	uint32_t obj_size;
	uint32_t addr;
	uint32_t init;
	uint8_t pad[20];
};

struct ft_storage {
	struct ft_storage_priv ft0;
	struct ft_storage_priv ft1;
	struct ft_storage_priv ft2;
	uint8_t buffer[256];
};

int
ft_storage_init(struct ft_storage *h, uint32_t obj_size, uint32_t addr);

int
ft_storage_read(struct ft_storage *h, void *out);

int
ft_storage_write(struct ft_storage *h, const void *b);

#endif
