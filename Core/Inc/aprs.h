/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APRS_H_
#define APRS_H_


#include <stdint.h>

#define TIME_LEN 7
#define LAT_LEN 8
#define LON_LEN 9
#define LATLON_EXT_LEN 7

int
aprs_telemetry_report(uint8_t *buff, uint8_t sequence_n, uint8_t analog1,
                      uint8_t analog2, uint8_t analog3, uint8_t analog4, uint8_t analog5,
                      uint8_t *digital, uint8_t *comment, uint8_t comment_len);

int
aprs_telemetry_parameter_name_message(uint8_t *buff, uint8_t *addressee,
                                      uint8_t names[][7], uint8_t *lengths);

int
aprs_latlong_position_report_data_timestamp(uint8_t *buff, uint8_t *lat,
                uint8_t *lon, uint8_t *extention, uint8_t *comment, uint8_t comment_len);

int
aprs_telemetry_unit_label_message(uint8_t *out, uint8_t *addressee,
                                  uint8_t units_labels[][7], uint8_t *lengths);


#endif /* APRS_H_ */
