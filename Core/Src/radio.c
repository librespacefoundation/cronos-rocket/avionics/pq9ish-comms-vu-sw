/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "radio.h"
#include <string.h>
#include <FreeRTOS.h>
#include "queue.h"
#include "watchdog.h"
#include "error.h"
#include "ax5043_driver.h"
#include "conf.h"
#include "utils.h"
#include "bsp_pq9ish_comms.h"
#include "pq9ish.h"
#include <Drivers/AX5043/include/fec.h>
#include <Drivers/AX5043/include/ccsds.h>
#include "crc.h"
#include "ax25.h"

extern struct pq9ish hpq9ish;
extern struct watchdog hwdg;
extern QueueHandle_t rx_queue;
extern QueueHandle_t tx_queue;

/* NOTE: These objects are quite large to fit inside the stack */
struct tx_frame tx_msg;
struct rx_frame rx_msg;
struct tx_frame
	tmp_tx_msg; // helper for transmiiting, only using its pdu and len fields

#define TX_BUFFER_LEN (10 * 2048 + 1024)

static uint8_t
__attribute__((section(".ram2"))) ax5043_rx[MAX_RX_FRAME_LEN];
static uint8_t ax5043_tx[TX_BUFFER_LEN];

ax25_conf_t hax25;

static int conf_times = 0;

int
radio_init(struct radio *hradio)
{
	int ret;
	if (!hradio) {
		return -INVAL_PARAM;
	}


	/* Configure XTAL and VCO */
	hradio->hax5043.vco = VCO_INTERNAL;
	hradio->hax5043.xtal.freq = PQ9ISH_XTAL_FREQ_HZ;
	hradio->hax5043.xtal.type = TCXO;
	hradio->hax5043.xtal.capacitance = 3;

	/* Setup AX5043 driver internal buffers */
	hradio->hax5043.rx_buffer = ax5043_rx;
	hradio->hax5043.rx_buffer_len = MAX_RX_FRAME_LEN;
	hradio->hax5043.tx_buffer = ax5043_tx;
	hradio->hax5043.tx_buffer_len = TX_BUFFER_LEN;

	ret = ax5043_init(&hradio->hax5043);
	if (ret) {
		return ret;
	}
	ret = ax5043_set_pinfuncpwramp(&hradio->hax5043, 0, 0, PWRAMP_OUTPUT_PWRAMP);
	if (ret) {
		return ret;
	}
	ret = ax5043_set_pwramp(&hradio->hax5043, PA_DISABLE);
	if (ret) {
		return ret;
	}
	/*
	 * Enable the RF switch, by setting properly the ANTSEL.
	 * Note that older versions of the PQ9ISH had this logic inverted
	 */
	ret = ax5043_set_pinfuncantsel(&hradio->hax5043, 0, 0, AX5043_RF_SWITCH_ENABLE);
	if (ret) {
		return ret;
	}

	return NO_ERROR;
}

void
radio_frame_received(const uint8_t *pdu, size_t len)
{
	if (!pdu || !rx_queue || len > MAX_RX_FRAME_LEN) {
		return;
	}
	rx_msg.len = len;
	memcpy(rx_msg.pdu, pdu, len);
	/*
	 * At this point we are inside an ISR. Decoding the data here, may
	 * introduce a significant amount of latency. Therefore, we push the
	 * data into the queue and we let the decoding a lower priority task.
	 * This will enable the AX5043 driver to process any consecutive frames.
	 */
	BaseType_t high_prio_woken = pdFALSE;
	xQueueSendFromISR(rx_queue, &rx_msg, &high_prio_woken);
}

void
radio_update_rx_stats(struct radio *hradio, const struct radio_decode *d)
{
	if (!hradio || !d) {
		return;
	}
	if (hradio->rx_frames < 0xFFFF) {
		hradio->rx_frames++;
	}

	if (d->crc_valid) {
		if (hradio->rx_corrected_bits < 0xFFFFFFFF) {
			hradio->rx_corrected_bits += d->corrected_bits;
		}
	} else {
		if (hradio->rx_frames_invalid < 0xFFFF) {
			hradio->rx_frames_invalid++;
		}
	}
}

int
radio_frame_decode(struct radio_decode *res, uint8_t *pdu, size_t len)
{
	if (!pdu || len < 32 + 4 || len > 255 || !res) {
		return INVAL_PARAM;
	}

	int err = rs_decode(pdu, len);
	if (err < 0) {
		res->crc_valid = 0;
		res->corrected_bits = 0;
		res->len = 0;
		return NO_ERROR;
	}

	/* Drop the parity symbols of RS */
	len -= 32;

	/*
	 * Check the CRC for the validity of the frame.
	 * PQ9ISH uses the Castagnoli CRC-32. This CRC is more suitable for
	 * small frames according to
	 * Koopman, Philip. "32-bit cyclic redundancy codes for internet
	 * applications." Proceedings International Conference on
	 * Dependable Systems and Networks. IEEE, 2002.
	 */

	res->corrected_bits = err;
	uint32_t crc_calc = crc32_c(pdu, len - sizeof(uint32_t));
	uint32_t crc_recv = (pdu[len - 4] << 24) | (pdu[len - 3] << 16)
	                    | (pdu[len - 2] << 8) | pdu[len - 1];
	if (crc_calc == crc_recv) {
		res->crc_valid = 1;
		res->len = len - 4;
	} else {
		res->crc_valid = 0;
		res->len = 0;
	}
	return NO_ERROR;
}


static uint32_t
baudrate(radio_baud_t b)
{
	switch (b) {
		case RADIO_BAUD_600:
			return 600;
		case RADIO_BAUD_1200:
			return 1200;
		case RADIO_BAUD_2400:
			return 2400;
		case RADIO_BAUD_4800:
			return 4800;
		case RADIO_BAUD_9600:
			return 9600;
		case RADIO_BAUD_19200:
			return 19200;
		case RADIO_BAUD_38400:
			return 38400;
		case RADIO_BAUD_125000:
			return 125000;
		default:
			return 0;
	}
	return 0;
}


static int
radio_start_rx(struct radio *hradio)
{
	int ret;
	ret = ax5043_config_freq(&hradio->hax5043, FREQA_MODE,
	                         hpq9ish.settings.rx_freq);
	if (ret) {
		return ret;
	}
	ret = ax5043_tune(&hradio->hax5043, FREQA_MODE, TUNE_RX);
	if (ret) {
		return ret;
	}

	/*
	 * Reception configuration parameters. They remain constant :( throughout
	 * the entire mission
	 */
	struct rx_params r = {
		.mod = FSK,
		.baudrate = 9600,
		.bandwidth = 2 * 9600,
		.freq_offset_corr = SECOND_LO,
		.max_rf_offset = 2000,
		.dr_offset = 100,
		.en_diversity = 0,
		.antesel = AX5043_RF_SWITCH_ENABLE,
		.fsk = {
			.mod_index = 1
		},
		.framing = RAW_PATTERN_MATCH,
		.raw_pattern = {
			.preamble = 0x33,
			.preamble_len = 8,
			.preamble_max = 14,
			.preamble_unencoded = 1,
			.sync = hpq9ish.settings.sat_id,
			.sync_len = 32,
			.sync_max = 30,
			.sync_unencoded = 1,
			.enc = {
				.inv = 0,
				.diff = 0,
				.scrambler = 0,
				.manch = 0,
				.nosync = 0
			},
			.crc = {
				.mode = CRC_OFF,
				.init = 0xFFFFFFFF
			},
			.pkt = {
				.addr_pos = 0,
				.fec_sync_dis = 1,
				.crc_skip_first = 1,
				.msb_first = 1,
				.len_pos = 0,
				.len_bits = 0,
				/* TC frame size + 32 RS parity + 4 CRC */
				.len_offset = 0, MAX_TC_FRAME_SIZE + 32 + 4,
				.max_len = 200,
				.addr = 0,
				.addr_mask = 0x0
			}
		}
	};

	ret = ax5043_conf_rx(&hradio->hax5043, &r);
	if (ret) {
		return ret;
	}

	/*
	 * All CRC and address match checks are performed by the MCU not the
	 * AX5043
	 */
	ax5043_set_pktacceptflags(&hradio->hax5043, 0b111100);

	ret = ax5043_start_rx(&hradio->hax5043);
	if (ret) {
		return ret;
	}
	hradio->state = RADIO_STATE_RX;
	return NO_ERROR;
}

static int
radio_stop_rx(struct radio *hradio)
{
	ax5043_update_rx_status(&hradio->hax5043);
	hradio->state = RADIO_STATE_POWER_DOWN;
	return ax5043_stop_rx(&hradio->hax5043, 1000);
}

/**
 * Return the signal path of the TX signal
 * @param enable_pa 1 to use the PA, 0 to bypass it
 * @return TXSE if the PA
 */
static rf_out_t
select_tx_path(uint8_t enable_pa)
{
#if PQ9ISH_BYPASS_PA == 0
	if (enable_pa) {
		return TXSE;
	}
#endif
	return TXDIFF;
}

static int
radio_conf_tx_fsk(struct radio *hradio, const struct tx_frame *msg)
{
	struct tx_params p = {
		.mod = FSK,
		.baudrate = baudrate(msg->meta.baud),
		.bandwidth = 2 * baudrate(msg->meta.baud),
		.rf_out_mode = select_tx_path(msg->meta.enable_pa),
		.shaping = UNSHAPED,
		.pout_dBm = msg->meta.tx_power,
		.fsk = {
			.mod_index = 1.0,
			.order = 1,
			.freq_shaping = GAUSIAN_BT_0_5
		},
		.framing = RAW_PATTERN_MATCH,
		.pattern = {
			.preamble = 0x33,
			.preamble_len = 64,
			.preamble_unencoded = 1,
			.sync = hpq9ish.settings.sat_id,
			.sync_len = 32,
			.sync_unencoded = 1,
			.enc = {
				.inv = 0,
				.diff = 0,
				.scrambler = 0,
				.manch = 0,
				.nosync = 0
			},
			.crc = {
				.mode = CRC_OFF,
				.init = 0xFFFFFFFF
			},
			.pkt = {
				.addr_pos = 0,
				.fec_sync_dis = 1,
				.crc_skip_first = 1,
				.msb_first = 1,
				.len_pos = 0,
				.len_bits = 0,
				.len_offset = 0,
				.max_len = 200,
				.addr = 0,
				.addr_mask = 0x0
			}
		}
	};
	/* CC requires special handling  */
	if (msg->meta.enc == RADIO_ENC_CC_1_2_RS) {
		p.pattern.preamble_len = 0;
		p.pattern.sync_len = 0;

	} else if (msg->meta.enc == RADIO_ENC_AX25) {
		p.framing = HDLC;
		p.hdlc.en_nrz = 0;
		p.hdlc.en_nrzi = 1;
		p.hdlc.en_scrambler = 1;
		p.hdlc.preamble_len = AX25_PREAMBLE_LEN;
		p.hdlc.postamble_len = AX25_POSTAMBLE_LEN;
	}
	return ax5043_conf_tx(&hradio->hax5043, &p);
}

static int
radio_conf_tx_bpsk(struct radio *hradio, const struct tx_frame *msg)
{
	struct tx_params p = {
		.mod = PSK,
		.baudrate = baudrate(msg->meta.baud),
		.bandwidth = 2 * baudrate(msg->meta.baud),
		.rf_out_mode = select_tx_path(msg->meta.enable_pa),
		.shaping = RC,
		.pout_dBm = msg->meta.tx_power,
		.psk = {
			.order = 1
		},
		.framing = RAW_PATTERN_MATCH,
		.pattern = {
			.preamble = 0x33,
			.preamble_len = 64,
			.preamble_unencoded = 1,
			.sync = hpq9ish.settings.sat_id,
			.sync_len = 32,
			.sync_unencoded = 1,
			.enc = {
				.inv = 0,
				.diff = 0,
				.scrambler = 0,
				.manch = 0,
				.nosync = 0
			},
			.crc = {
				.mode = CRC_OFF,
				.init = 0xFFFFFFFF
			},
			.pkt = {
				.addr_pos = 0,
				.fec_sync_dis = 1,
				.crc_skip_first = 1,
				.msb_first = 1,
				.len_pos = 0,
				.len_bits = 0,
				.len_offset = 0,
				.max_len = 200,
				.addr = 0,
				.addr_mask = 0x0
			}
		}
	};
	/* CC requires special handling  */
	if (msg->meta.enc == RADIO_ENC_CC_1_2_RS) {
		p.pattern.preamble_len = 0;
		p.pattern.sync_len = 0;
	} else if (msg->meta.enc == RADIO_ENC_AX25) {
		p.framing = HDLC;
		p.hdlc.en_nrz = 0;
		p.hdlc.en_nrzi = 1;
		p.hdlc.en_scrambler = 1;
		p.hdlc.preamble_len = AX25_PREAMBLE_LEN;
		p.hdlc.postamble_len = AX25_POSTAMBLE_LEN;
	}
	return ax5043_conf_tx(&hradio->hax5043, &p);
}


static int
radio_conf_tx_bpsk_res(struct radio *hradio, const struct tx_frame *msg)
{
	struct tx_params p = {
		.mod = OQPSK,
		.baudrate = baudrate(msg->meta.baud),
		.bandwidth = 2 * baudrate(msg->meta.baud),
		.rf_out_mode = select_tx_path(msg->meta.enable_pa),
		.shaping = RC,
		.pout_dBm = msg->meta.tx_power,
		.framing = RAW_PATTERN_MATCH,
		.pattern = {
			.preamble = 0x33,
			.preamble_len = 0,
			.preamble_unencoded = 1,
			.sync = hpq9ish.settings.sat_id,
			.sync_len = 0,
			.sync_unencoded = 1,
			.enc = {
				.inv = 0,
				.diff = 0,
				.scrambler = 0,
				.manch = 0,
				.nosync = 0
			},
			.crc = {
				.mode = CRC_OFF,
				.init = 0xFFFFFFFF
			},
			.pkt = {
				.addr_pos = 0,
				.fec_sync_dis = 1,
				.crc_skip_first = 1,
				.msb_first = 1,
				.len_pos = 0,
				.len_bits = 0,
				.len_offset = 0,
				.max_len = 0,
				.addr = 0,
				.addr_mask = 0x0
			}
		}
	};
	return ax5043_conf_tx(&hradio->hax5043, &p);
}

static int
radio_conf_tx(struct radio *hradio, const struct tx_frame *msg)
{
	switch (msg->meta.mod) {
		case RADIO_MOD_FSK:
			return radio_conf_tx_fsk(hradio, msg);
		case RADIO_MOD_BPSK:
			return radio_conf_tx_bpsk(hradio, msg);
		case RADIO_MOD_BPSK_RES:
			return radio_conf_tx_bpsk_res(hradio, msg);
		default:
			return -PQ9ISH_NOT_IMPL;
	}
	return -INVAL_PARAM;
}

/**
 * Hack the QPSK and use only two from the four symbols. This will create a
 * DC bias and a DC spike will appear in the middle of the spectrum.
 *
 * This can be used as a residual carrier.
 *
 * @param out the output buffer. Should have a length of 2 times the input frame
 * length
 * @param frame the input data
 * @param length the length of the input data in bytes
 */
void
bpsk_res_precode(uint8_t *out, const uint8_t *frame, size_t length)
{
	uint8_t b = 0;
	const uint8_t map[2] = {0x0, 0x2};
	for (size_t i = 0; i < length * 8; i++) {
		b = (frame[i / 8] >> (7 - (i % 8))) & 0x01;
		out[i / 4] = (out[i / 4] << 2) | map[b];
	}
}


/**
 * We cannot use the hardware differential encoder, because it degrades performance
 * if it is performed after the convolutional coding
 * @param out output buffer
 * @param in input buffer
 * @param len the size of the input buffer
 */
static void
diff_encoder(uint8_t *out, const uint8_t *in, uint32_t len)
{
	uint8_t prev = 0;
	for (uint32_t i = 0; i < len; i++) {
		uint8_t b7 = in[i] >> 7;
		uint8_t b6 = (in[i] >> 6) & 0x1;
		uint8_t b5 = (in[i] >> 5) & 0x1;
		uint8_t b4 = (in[i] >> 4) & 0x1;
		uint8_t b3 = (in[i] >> 3) & 0x1;
		uint8_t b2 = (in[i] >> 2) & 0x1;
		uint8_t b1 = (in[i] >> 1) & 0x1;
		uint8_t b0 = in[i] & 1;
		prev = b7 ^ prev;
		out[i] = prev << 7;

		prev = b6 ^ prev;
		out[i] |= prev << 6;

		prev = b5 ^ prev;
		out[i] |= prev << 5;

		prev = b4 ^ prev;
		out[i] |= prev << 4;

		prev = b3 ^ prev;
		out[i] |= prev << 3;

		prev = b2 ^ prev;
		out[i] |= prev << 2;

		prev = b1 ^ prev;
		out[i] |= prev << 1;

		prev = b0 ^ prev;
		out[i] |= prev;
	}
}

static int
append_crc(struct tx_frame *msg)
{
	/* Calculate and append CRC */
	uint32_t c = crc32_c(msg->pdu, msg->len);
	msg->pdu[msg->len] = c >> 24;
	msg->pdu[msg->len + 1] = c >> 16;
	msg->pdu[msg->len + 2] = c >> 8;
	msg->pdu[msg->len + 3] = c;
	msg->len += sizeof(uint32_t);
	return NO_ERROR;
}

static int
radio_frame_tx(struct radio *hradio, struct tx_frame *msg)
{
	int ret;
	hradio->state = RADIO_STATE_TX;

	switch (msg->meta.enc) {
		case RADIO_ENC_RAW:
			append_crc(msg);
			ccsds_scrambler(msg->pdu, msg->len);
			memcpy(tmp_tx_msg.pdu, msg->pdu,
			       msg->len + POSTAMBLE_BYTES);
			tmp_tx_msg.len = msg->len + POSTAMBLE_BYTES;
			break;
		case RADIO_ENC_RS:
			append_crc(msg);
			ccsds_scrambler(msg->pdu, msg->len);
			memcpy(tmp_tx_msg.pdu, msg->pdu, msg->len);
			rs_encode(tmp_tx_msg.pdu + msg->len,
			          tmp_tx_msg.pdu, msg->len);
			tmp_tx_msg.len = msg->len + 32 + POSTAMBLE_BYTES;
			break;
		/*
		 * This needs also a bit of special handling, as the CCSDS
		 * SFD should be encoded
		 */
		case RADIO_ENC_CC_1_2_RS:
			append_crc(msg);
			ccsds_scrambler(msg->pdu, msg->len);
			memset(tmp_tx_msg.pdu, 0x33, 4);
			tmp_tx_msg.len = 4;
			tmp_tx_msg.pdu[tmp_tx_msg.len++] = hpq9ish.settings.sat_id >> 24;
			tmp_tx_msg.pdu[tmp_tx_msg.len++] = hpq9ish.settings.sat_id >> 16;
			tmp_tx_msg.pdu[tmp_tx_msg.len++] = hpq9ish.settings.sat_id >> 8;
			tmp_tx_msg.pdu[tmp_tx_msg.len++] = hpq9ish.settings.sat_id;

			memcpy(tmp_tx_msg.pdu + tmp_tx_msg.len,
			       msg->pdu, msg->len);
			tmp_tx_msg.len += msg->len;

			/* Perform RS */
			rs_encode(tmp_tx_msg.pdu + tmp_tx_msg.len,
			          msg->pdu, msg->len);
			tmp_tx_msg.len += 32;

			/*
			 * Append a zero byte (more than 6 zero bits), to ensure
			 * the reset of the convolutional encoder
			 */
			tmp_tx_msg.pdu[tmp_tx_msg.len++] = 0;
			diff_encoder(tmp_tx_msg.pdu, tmp_tx_msg.pdu, tmp_tx_msg.len);
			conv_encoder_1_2_7(msg->pdu, tmp_tx_msg.pdu,
			                   tmp_tx_msg.len);
			/* For concinstency copy back the result */
			memcpy(tmp_tx_msg.pdu, msg->pdu, 2 * tmp_tx_msg.len);
			tmp_tx_msg.len *= 2;
			break;
		case RADIO_ENC_AX25:
			ret = ax25_init(&hax25, CALLSIGN_DESTINATION, SSID_DESTINATION,
			                CALLSIGN_STATION, SSID_STATION,
			                AX25_UI_FRAME, 0x3, 0xF0);
			ret = ax25_prepare_frame(hradio, msg, &hax25);
			break;
		default:
			return -INVAL_PARAM;
	}

	/* Apply precoding so the residual carrier can appear */
	if (msg->meta.mod == RADIO_MOD_BPSK_RES) {
		/* CC=1/2 RS(223, 255) already contains the encoded preamble */
		uint32_t len = 0;
		if (msg->meta.enc == RADIO_ENC_CC_1_2_RS) {
			memcpy(msg->pdu, tmp_tx_msg.pdu, tmp_tx_msg.len);
			len = tmp_tx_msg.len;
		} else {
			msg->pdu[len++] = hpq9ish.settings.sat_id >> 24;
			msg->pdu[len++] = hpq9ish.settings.sat_id >> 16;
			msg->pdu[len++] = hpq9ish.settings.sat_id >> 8;
			msg->pdu[len++] = hpq9ish.settings.sat_id;
			memcpy(msg->pdu + len, tmp_tx_msg.pdu, tmp_tx_msg.len);
			len += tmp_tx_msg.len;
		}
		bpsk_res_precode(tmp_tx_msg.pdu, msg->pdu, len);
		tmp_tx_msg.len *= 2;
	}

	return ax5043_tx_frame(&hradio->hax5043, tmp_tx_msg.pdu,
	                       tmp_tx_msg.len, 1000);
}


static int
radio_tx(struct radio *hradio, struct tx_frame *msg)
{
	int ret;

	/*
	 * Wait for the previous TX to complete before reconfiguring the
	 * hardware
	 */
	while (hradio->state != RADIO_STATE_POWER_DOWN) {
		osDelay(1);
	}

	/*
	 * FIXME: Reconfiguration should be avoided for back2back frames
	 * using the same PHY params
	 */
//	if (conf_times == 0) {
//		ret = ax5043_config_freq(&hradio->hax5043, FREQA_MODE,
//								 hpq9ish.settings.tx_freq);
//		if (ret) {
//			Error_Handler();
//		}
//		conf_times++;
//	}

	ret = ax5043_tune(&hradio->hax5043, FREQA_MODE, TUNE_TX);
	if (ret) {
		Error_Handler();
	}

	ret = radio_conf_tx(hradio, msg);
	if (ret) {
		return ret;
	}

	if (msg->meta.enable_pa) {
#if PQ9ISH_BYPASS_PA == 0
		ax5043_set_pwramp(&hradio->hax5043, PA_ENABLE);
#endif
	}
	ret = radio_frame_tx(hradio, msg);
	if (ret) {
		radio_tx_complete();
	}
	return NO_ERROR;
}

void
radio_tx_complete()
{
	/* Update the TX frames counter */
	if (hpq9ish.hradio.tx_frames < 0xFFFF) {
		hpq9ish.hradio.tx_frames++;
	}
	/* Explicitly disable the PA */
	ax5043_set_pwramp(&hpq9ish.hradio.hax5043, PA_DISABLE);
	hpq9ish.hradio.state = RADIO_STATE_POWER_DOWN;
}

/**
 * The TX task is responsible for sending frames to the earth using the
 * AX5043 IC. To do so, it dequeues frames from the \p tx_queue. Tasks that
 * should send data to the ground station shall use the message queue and NOT
 * the AX5043 IC directly.
 */
void
radio_task()
{
	struct radio *hradio = &hpq9ish.hradio;
	uint8_t wdgid = 0;
	int ret = 1;
	/* Try to register to the watchdog */
	while (ret) {
		ret = watchdog_register(&hwdg, &wdgid, "radio_tx");
	}

	/*
	 * Start the RX. It maybe interrupted only if a frame shall be
	 * transmitted back to earth
	 */
	ret = radio_start_rx(hradio);
	if (ret) {
		Error_Handler();
	}
	while (1) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		if (xQueueReceive(tx_queue, &tx_msg, pdMS_TO_TICKS(5)) == pdPASS) {
			/*
			 * When there is a request for TX the RX stops. In order
			 * to avoid continuous reconfiguration of the IC, the
			 * RX will start after the timeout of the queue and only
			 * if the IC is not already in RX
			 */
			if (hradio->state == RADIO_STATE_RX) {
				ret = radio_stop_rx(hradio);
				if (ret) {
					Error_Handler();
				}
			}

			ret = radio_tx(hradio, &tx_msg);


		} else {
			if (hradio->state == RADIO_STATE_POWER_DOWN) {
				ret = radio_start_rx(hradio);
				if (ret) {
					Error_Handler();
				}
			}
		}
	}
}
