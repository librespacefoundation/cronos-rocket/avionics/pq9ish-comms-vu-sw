/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Functions based on APRS1.1 standard
 * Pages inside the comments of each function refer to this pdf
 * http://www.aprs.org/doc/APRS101.PDF
*/

#include"aprs.h"
#include"pq9ish.h"
#include "main.h"
#include "error.h"
#include <string.h>
#include <stdio.h>

static int
aprs_get_time(uint8_t *out);

/**
 * Formats data into an APRS telemetry report message
 * @param out the output buffer
 * @param sequence_n sequence number
 * @param analog1 analog value 1
 * @param analog2 analog value 2
 * @param analog3 analog value 3
 * @param analog4 analog value 4
 * @param analog5 analog value 5
 * @param digital 8 digital values (8 bits)
 * @param comment comment message
 * @param comment_len comment length
 * @return
 */

int
aprs_telemetry_report(uint8_t *out, uint8_t sequence_n, uint8_t analog1,
                      uint8_t analog2, uint8_t analog3, uint8_t analog4, uint8_t analog5,
                      uint8_t *digital, uint8_t *comment, uint8_t comment_len)
{
	/* p.68 */
	uint8_t idx = 0;
	idx += sprintf((char *)(out) + idx, "T#%03u,%03u,%03u,%03u,%03u,%03u,",
	               sequence_n,
	               analog1,
	               analog2, analog3, analog4, analog5);
	memcpy(out + idx, digital, 8);
	idx += 8;
	memcpy(out + idx, comment, comment_len);
	idx += comment_len;
	return idx;

}

/**
 * Formats names into an APRS telemetry parameter name message
 * @param out output buffer
 * @param addressee addressee string (9characters)
 * @param names the names of each data channel
 * @param lengths the length of each name
 * @return
 */
int
aprs_telemetry_parameter_name_message(uint8_t *out, uint8_t *addressee,
                                      uint8_t names[][7], uint8_t *lengths)
{
	/* p.69 */
	//TODO: add check if lengths are valid
	uint8_t idx = 0;
	out[idx++] = ':';
	memcpy(out + idx, addressee, 9);
	idx += 9;
	idx += sprintf((char *)(out) + idx, ":PARM.");
	for (uint8_t i = 0; i < 12; i++) {
		memcpy(out + idx, names[i], lengths[i]);
		idx += lengths[i];
		out[idx++] = ',';
	}
	memcpy(out + idx, names[12], lengths[12]);
	idx += lengths[12];
	return idx;
}
/**
 *
 * @param out
 * @param addressee
 * @param units_labels Five first are units for the analog values, eight next are labels for the digital
 * @param lengths for each of the unit/label strings
 * @return
 */
int
aprs_telemetry_unit_label_message(uint8_t *out, uint8_t *addressee,
                                  uint8_t units_labels[][7], uint8_t *lengths)
{
	/* p.69 */
	//TODO: add check if lengths are valid
	uint8_t idx = 0;
	out[idx++] = ':';
	memcpy(out + idx, addressee, 9);
	idx += 9;
	idx += sprintf((char *)(out) + idx, ":UNIT.");
	for (uint8_t i = 0; i < 12; i++) {
		memcpy(out + idx, units_labels[i], lengths[i]);
		idx += lengths[i];
		out[idx++] = ',';
	}
	memcpy(out + idx, units_labels[12], lengths[12]);
	idx += lengths[12];
	return idx;
}


/**
 * Formats coordinates into an APRS Lat/Long Position Report Format — with Data Extension and Timestamp
 * @param out The output buffer
 * @param lat lattitude in DDMM.MM format
 * @param lon longtitude in DDDMM.MM format
 * @param extention Data Extension , can be: Course/Speed or Power/Height/Gain/Dir or Radio Range or DF Signal Strength
 * @param comment comment message
 * @param comment_len comment message length
 * @return
 */
int
aprs_latlong_position_report_data_timestamp(uint8_t *out, uint8_t *lat,
                uint8_t *lon, uint8_t *extention, uint8_t *comment, uint8_t comment_len)
{
	/* p.33 */
	if (comment_len > 36) {
		//comment should be upto 36 characters
		return -INVAL_PARAM;
	}

	uint8_t idx = 0;
	uint8_t timestamp[8] ;
	uint8_t ret = aprs_get_time(timestamp);
	if (ret) {
		return ret;
	}

	out[idx++] = '@';
	memcpy(out + idx, timestamp, TIME_LEN);
	idx += TIME_LEN;
	memcpy(out + idx, lat, LAT_LEN);
	idx += LAT_LEN;
	out[idx++] = '/';
	memcpy(out + idx, lon, LON_LEN);
	idx += LON_LEN;
	out[idx++] = '>';
	memcpy(out + idx, extention, LATLON_EXT_LEN);
	idx += LATLON_EXT_LEN;
	memcpy(out + idx, comment, comment_len);
	idx += comment_len;
	return idx;
}

/*
 * helper function to create timestamps used in APRS packets
 */
static int
aprs_get_time(uint8_t *out)
{
	extern RTC_HandleTypeDef hrtc;
	int ret;
	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	if (!out) {
		return -INVAL_PARAM;
	}
	ret = HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
	if (ret) {
		return ret;
	}
	ret = HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);
	if (ret) {
		return ret;
	}
	sprintf((char *)out, "%02u", date.Date);
	sprintf((char *)out + 2, "%02u", time.Hours);
	sprintf((char *)out + 4, "%02u", time.Minutes);
	out[6] = 'z';
	out[7] = 0;
	return NO_ERROR;
}

