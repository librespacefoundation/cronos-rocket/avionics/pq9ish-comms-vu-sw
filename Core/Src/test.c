/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test.h"
#include "watchdog.h"
#include "conf.h"
#include <cmsis_os.h>
#include <Drivers/AX5043/include/ax5043.h>
#include "bsp_pq9ish_comms.h"
#include "osdlp_queue_handle.h"
#include "pq9ish.h"
#include "aprs.h"
#include <stdio.h>
#include <string.h>

#define TEST_TX_QUEUE 1
extern struct pq9ish hpq9ish;
extern struct watchdog hwdg;
extern QueueHandle_t tx_queue;

#if AX5043_DEBUG
#define TRACE_MAX_LEN 3200
static char regs_str[TRACE_MAX_LEN];
#endif

static void
trace_printf(const char *s, size_t slen)
{
	if (!s) {
		return;
	}
	for (size_t i = 0; i < slen; i++) {
		if (s[i] == 0) {
			break;
		}
		ITM_SendChar(s[i]);
	}
}

static void
test_rx(freq_mode_t fmode, uint32_t freq, uint32_t duration_ms)
{
	int ret;
	struct radio *hradio = &hpq9ish.hradio;
	ret = ax5043_config_freq(&hradio->hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hradio->hax5043, fmode, TUNE_RX);
	if (ret) {
		Error_Handler();
	}

	struct rx_params r = {
		.mod = FSK,
		.baudrate = 9600,
		.bandwidth = 17400,
		.freq_offset_corr = SECOND_LO,
		.max_rf_offset = 2000,
		.dr_offset = 100,
		.en_diversity = 0,
		.antesel = AX5043_RF_SWITCH_ENABLE,
		.fsk = {
			.mod_index = 1
		},
		.framing = RAW_PATTERN_MATCH,
		.raw_pattern = {
			.preamble = 0x3333,
			.preamble_len = 8 * 8,
			.preamble_max = 14,
			.preamble_unencoded = 1,
			.sync = 0x1ACFFC1D,
			.sync_len = 32,
			.sync_max = 28,
			.sync_unencoded = 1,
			.enc = {
				.inv = 0,
				.diff = 0,
				.scrambler = 0,
				.manch = 0,
				.nosync = 0
			},
			.crc = {
				.mode = CRC32,
				.init = 0xFFFFFFFF
			},
			.pkt = {
				.addr_pos = 0,
				.fec_sync_dis = 0,
				.crc_skip_first = 0,
				.msb_first = 1,
				.len_pos = 0,
				.len_bits = 0,
				.len_offset = 64,
				.max_len = 200,
				.addr = 0,
				.addr_mask = 0x0
			}
		}
	};

	/* Test RX */
	ret = ax5043_conf_rx(&hradio->hax5043, &r);
	if (ret) {
		Error_Handler();
	}

	ax5043_set_pktacceptflags(&hradio->hax5043, 0b111100);

	ret = ax5043_start_rx(&hradio->hax5043);
	if (ret) {
		Error_Handler();
	}

//	for (size_t i = 0; i < duration_ms / 250; i++) {
//		ret = ax5043_update_rx_status(&hradio->hax5043);
//		if (ret) {
//			Error_Handler();
//		}
//		osDelay(250);
//	}
	osDelay(duration_ms);

	ret = ax5043_stop_rx(&hradio->hax5043, 1000);
	if (ret) {
		Error_Handler();
	}
}


static void
test_cw(freq_mode_t fmode, uint32_t freq, uint32_t duration_ms)
{
	int ret;
	struct radio *hradio = &hpq9ish.hradio;
	ret = ax5043_config_freq(&hradio->hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hradio->hax5043, fmode, TUNE_TX);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_conf_cw(&hradio->hax5043);
	if (ret) {
		Error_Handler();
	}

	/* Going for TX */
	ret = ax5043_set_pwramp(&hradio->hax5043, PA_ENABLE);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tx_cw(&hradio->hax5043, duration_ms);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_set_pwramp(&hradio->hax5043, PA_DISABLE);
	if (ret) {
		Error_Handler();
	}
}


static void
test_fsk_ax25(freq_mode_t fmode, uint32_t freq, uint32_t baud,
              uint32_t nframes, uint32_t delay_ms, uint8_t wdid)
{
	int ret;
	struct radio *hradio = &hpq9ish.hradio;
	ret = ax5043_config_freq(&hradio->hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hradio->hax5043, fmode, TUNE_TX);
	if (ret) {
		Error_Handler();
	}
	struct tx_params p = {
		.mod = FSK,
		.baudrate = baud,
		.bandwidth = 2 * baud,
		.rf_out_mode = TXSE,
		.shaping = UNSHAPED,
		.pout_dBm = 16.0f,
		.fsk = {
			.mod_index = 1.0,
			.order = 1,
			.freq_shaping = GAUSIAN_BT_0_5
		},
		.framing = HDLC,
		.hdlc = {
			.en_nrz = 0,
			.en_nrzi = 1,
			.en_scrambler = 1,
			.preamble_len = 32,
			.postamble_len = 4
		}
	};
	ret = ax5043_conf_tx(&hradio->hax5043, &p);
	if (ret) {
		Error_Handler();
	}
	for (uint32_t i = 0; i < nframes; i++) {
		/* Read random memory and transmit it */
		ax5043_tx_frame(&hradio->hax5043, (const uint8_t *)&p, sizeof(p), 4000);
		watchdog_reset_subsystem(&hwdg, wdid);
		osDelay(delay_ms);
	}
}

static void
test_bpsk_ax25(freq_mode_t fmode, uint32_t freq, uint32_t baud,
               uint32_t nframes, uint32_t delay_ms, uint8_t wdid)
{
	int ret;
	struct radio *hradio = &hpq9ish.hradio;
	ret = ax5043_config_freq(&hradio->hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hradio->hax5043, fmode, TUNE_TX);
	if (ret) {
		Error_Handler();
	}
	struct tx_params p = {
		.mod = PSK,
		.baudrate = baud,
		.bandwidth = 2 * baud,
		.rf_out_mode = TXSE,
		.shaping = RC,
		.pout_dBm = 16.0f,
		.psk = {
			.order = 1
		},
		.framing = HDLC,
		.hdlc = {
			.en_nrz = 0,
			.en_nrzi = 1,
			.en_scrambler = 1,
			.preamble_len = 64,
			.postamble_len = 4
		}
	};
	ret = ax5043_conf_tx(&hradio->hax5043, &p);
	if (ret) {
		Error_Handler();
	}
	for (uint32_t i = 0; i < nframes; i++) {
		/* Read random memory and transmit it */
		ax5043_tx_frame(&hradio->hax5043, (const uint8_t *)&p, sizeof(p), 4000);
		watchdog_reset_subsystem(&hwdg, wdid);
		osDelay(delay_ms);
	}
}

struct tx_frame tx;
static void
test_tx_queue(freq_mode_t fmode, uint32_t freq, uint32_t baud,
              uint32_t nframes, uint32_t delay_ms, uint8_t wdid)
{
	int ret;
	struct radio *hradio = &hpq9ish.hradio;
	ret = ax5043_config_freq(&hradio->hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hradio->hax5043, fmode, TUNE_TX);
	if (ret) {
		Error_Handler();
	}
	struct tx_params p = {
		.mod = FSK,
		.baudrate = baud,
		.bandwidth = 2 * baud,
		.rf_out_mode = TXSE,
		.shaping = UNSHAPED,
		.pout_dBm = -2,
		.fsk = {
			.mod_index = 1.0,
			.order = 1,
			.freq_shaping = GAUSIAN_BT_0_5
		},
		.framing = RAW_PATTERN_MATCH,
		.pattern = {
			.preamble = 0x33,
			.preamble_len = 512,
			.preamble_unencoded = 1,
			.sync = 0x1ACFFC1D,
			.sync_len = 32,
			.sync_unencoded = 1,
			.enc = {
				.inv = 0,
				.diff = 0,
				.scrambler = 0,
				.manch = 0,
				.nosync = 0
			},
			.crc = {
				.mode = CRC_OFF,
				.init = 0xFFFFFFFF
			},
			.pkt = {
				.addr_pos = 0,
				.fec_sync_dis = 1,
				.crc_skip_first = 1,
				.msb_first = 1,
				.len_pos = 0,
				.len_bits = 8,
				.len_offset = 0,
				.max_len = 255,
				.addr = 0,
				.addr_mask = 0xFF
			}
		}
	};
	ret = ax5043_conf_tx(&hradio->hax5043, &p);
	if (ret) {
		Error_Handler();
	}
	memset(tx.pdu, 0xB5, 512);
	for (uint32_t i = 0; i < nframes; i++) {
		tx.len = 120;
		tx.timeout_ms = 5000;
		while (xQueueSend(tx_queue, &tx, pdMS_TO_TICKS(1000)) != pdPASS) {
			watchdog_reset_subsystem(&hwdg, wdid);
		}
		watchdog_reset_subsystem(&hwdg, wdid);
		osDelay(delay_ms);
	}
}

int
test_osdlp()
{
	initialize_osdlp();
	return 0;
}


static void
test_ax25_high_level(uint32_t delay_ms)
{
	tx.meta.baud = RADIO_BAUD_9600;
	tx.meta.enc = RADIO_ENC_AX25;
	tx.meta.mod = RADIO_MOD_FSK;
	tx.meta.tx_power = -2;
	tx.meta.enable_pa = 1;
	tx.len = sprintf((char *)tx.pdu,
	                 "This is a test. Framing follows AX25 protocol. Baudrate is 9600. Modulation is FSK.");
	tx.timeout_ms = 1000;

	extern QueueHandle_t tx_queue;
	xQueueSend(tx_queue, &tx, pdMS_TO_TICKS(5000));
	osDelay(delay_ms);
	return;
}

static void
test_aprs_location(uint32_t delay_ms, uint8_t wdgid)
{
	extern QueueHandle_t tx_queue;
	tx.meta.baud = RADIO_BAUD_9600;
	tx.meta.enc = RADIO_ENC_AX25;
	tx.meta.mod = RADIO_MOD_FSK;
	tx.meta.tx_power = -2;
	tx.meta.enable_pa = 1;
	//lat-long packet
	uint8_t lat[8] = "3801.01N"; //hackerspace.gr location :)
	uint8_t lon[9] = "02343.87E";
	uint8_t extention[7] = "088/036";
	uint8_t comments[32] = "THESE_ARE_EXTRA_COMMENTS_a1_Test";
	tx.len = aprs_latlong_position_report_data_timestamp(tx.pdu, lat, lon,
	                extention, comments, 32);
	tx.timeout_ms = 1000;
	xQueueSend(tx_queue, &tx, pdMS_TO_TICKS(5000));
	watchdog_reset_subsystem(&hwdg, wdgid);
	osDelay(delay_ms);
	return;
}


static void
test_aprs_telemetry(uint32_t delay_ms, uint8_t wdgid)
{
	extern QueueHandle_t tx_queue;
	tx.meta.baud = RADIO_BAUD_9600;
	tx.meta.enc = RADIO_ENC_AX25;
	tx.meta.mod = RADIO_MOD_FSK;
	tx.meta.tx_power = -2;
	tx.meta.enable_pa = 1;
	//telemetry packet
	uint8_t comments[32] = "THESE_ARE_EXTRA_COMMENTS_a1_Test";
	uint8_t digital[8] = "01011011";
	tx.len = aprs_telemetry_report(tx.pdu, 0, 12, 255, 0, 1, 2, digital, comments,
	                               32);
	xQueueSend(tx_queue, &tx, pdMS_TO_TICKS(5000));
	watchdog_reset_subsystem(&hwdg, wdgid);
	osDelay(delay_ms);

	//telemetry names packet
	uint8_t lengths[13] = {7, 6, 5, 5, 4, 5, 4, 3, 2, 2, 2, 2, 2 };
	uint8_t names[13][7] = { "ABCDEFG", "ABCDEFG", "ABCDEF", "ABCDEF", "ABCDE", "ABCDEF", "ABCDE", "ABCD", "ABCD", "ABCD", "ABC", "ABC", "ABC"};
	uint8_t addee[10] = "NOCALL-11";
	tx.len = aprs_telemetry_parameter_name_message(tx.pdu, addee, names, lengths);
	xQueueSend(tx_queue, &tx, pdMS_TO_TICKS(5000));
	watchdog_reset_subsystem(&hwdg, wdgid);
	osDelay(delay_ms);

	//telemetry units-labels packet
	uint8_t llengths[13] = {7, 6, 5, 5, 4, 5, 4, 3, 2, 2, 2, 2, 2 };
	uint8_t units_labels[13][7] = { "rad/sec", "unit12", "deg.F", "ABCDEF", "ABCDE", "ABCDEF", "ABCDE", "ABCD", "ABCD", "ABCD", "ABC", "ABC", "ABC"};
	//uint8_t addee[10] = "NOCALL-11";
	tx.len = aprs_telemetry_unit_label_message(tx.pdu, addee, units_labels,
	                llengths);
	xQueueSend(tx_queue, &tx, pdMS_TO_TICKS(5000));
	watchdog_reset_subsystem(&hwdg, wdgid);
	osDelay(delay_ms);

	return;
}




int
test_task()
{
	uint8_t wdgid = 0;
	/***************************************************************************
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * THIS TASK IS WATCHDOG ENABLED. TAKE CARE SO YOUR TESTS DO NOT EXECUTE
	 * LONGER THAN THE TIMEOUT OF THE IWDG
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	int ret = watchdog_register(&hwdg, &wdgid, "test");
	if (ret) {
		return ret;
	}
	osDelay(1000);


	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
#if TEST_OSDLP
		test_osdlp();
#endif
#if TEST_RX
		test_rx(FREQA_MODE, 433200000, 10000);
#if AX5043_DEBUG
		ax5043_dump_regs(&hpq9ish.hradio.hax5043);
		ax5043_dump_regs_str(regs_str, &hpq9ish.hradio.hax5043, TRACE_MAX_LEN);
		trace_printf("RX=", 4);
		trace_printf(regs_str, TRACE_MAX_LEN);
#endif
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_CW
		test_cw(FREQA_MODE, 434200000, 5000);
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_FSK_AX25
		test_fsk_ax25(FREQA_MODE, 433200000, 9600, 10, 1000, wdgid);
#if AX5043_DEBUG
		ax5043_dump_regs(&hpq9ish.hradio.hax5043);
		ax5043_dump_regs_str(regs_str, &hpq9ish.hradio.hax5043, TRACE_MAX_LEN);
		trace_printf("TX_FSK=", 8);
		trace_printf(regs_str, TRACE_MAX_LEN);
#endif
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_BSPK_AX25
		test_bpsk_ax25(FREQA_MODE, 433200000, 9600, 10, 1000, wdgid);
#if AX5043_DEBUG
		ax5043_dump_regs(&hpq9ish.hradio.hax5043);
		ax5043_dump_regs_str(regs_str, &hpq9ish.hradio.hax5043, TRACE_MAX_LEN);
		trace_printf("TX_BPSK=", 9);
		trace_printf(regs_str, TRACE_MAX_LEN);
#endif
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_AX25_HIGH_LEVEL
		test_ax25_high_level(1000);
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_APRS
		test_aprs_location(3000, wdgid);
		test_aprs_telemetry(3000, wdgid);
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_TX_QUEUE
		test_tx_queue(FREQA_MODE, 433200000, 9600, 10, 1000, wdgid);
#endif
		osDelay(1000);
	}
	return 0;
}
