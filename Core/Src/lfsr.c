/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lfsr.h"

static uint32_t
bitcnt(uint32_t x)
{
	/* Kudos https://graphics.stanford.edu/~seander/bithacks.html */
	uint32_t r = x - ((x >> 1) & 033333333333) - ((x >> 2) & 011111111111);
	return ((r + (r >> 3)) & 030707070707) % 63;
}

/**
 * Initializes the linear feedback shift register
 * @param h the LFDR handle
 * @param mask the polynomial mask
 * @param seed the initial seed
 * @param len the length of the LFSR. This equals the number of memory stages
 * minus 1.
 */
void
lfsr_init(struct lfsr *h, uint32_t mask, uint32_t seed, uint8_t len)
{
	if (!h || len > 31) {
		return;
	}
	h->len = len;
	h->sr = seed;
	h->mask = mask;
}

/**
 * Shifts by one bit the LFSR
 * @param h the LFSR handle
 * @return the content of the LS memory stage (oldest)
 */
uint8_t
lfsr_shift(struct lfsr *h)
{
	uint8_t b = h->sr & 0x1;
	uint32_t n = bitcnt(h->sr & h->mask) & 0x1;
	h->sr = (h->sr >> 1) | (n << h->len);
	return b;
}
