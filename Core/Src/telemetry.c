/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  @file telemetry.c
 *
 *  @date Jul 24, 2020
 *  @brief Telemetry data functions
 */

#include <telemetry.h>
#include <error.h>
#include <string.h>

#if defined(OSDLP_RX_ENABLE) || defined(OSDLP_TX_ENABLE)
#include <osdlp.h>
#include <osdlp_queue_handle.h>
#endif /* if OSDLP_RX_ENABLE || OSDLP_TX_ENABLE*/
#include <string.h>
#include <cmsis_os.h>

static struct tx_frame tmp_frame;
#if defined(OSDLP_RX_ENABLE) || defined(OSDLP_TX_ENABLE)
extern osMutexId osdlp_tx_mtxHandle;
uint8_t tm_proxy[OSDLP_MAX_TM_PACKET_LENGTH];
#endif /* if OSDLP_RX_ENABLE || OSDLP_TX_ENABLE*/

#if defined(OSDLP_RX_ENABLE) || defined(OSDLP_TX_ENABLE)
int
transmit_tm(const uint8_t *pkt, uint16_t length, uint8_t vcid,
            const struct tx_frame_metadata *meta)
{
	if (pkt == NULL || meta == NULL) {
		return -OSDLP_NULL;
	}
	struct tm_transfer_frame *tm;
	struct tc_transfer_frame *tc;
	int ret = 0;
	if (osMutexWait(osdlp_tx_mtxHandle, 200) == osOK) {
		ret = tm_get_tx_config(&tm, vcid);
		if (ret) {
			osMutexRelease(osdlp_tx_mtxHandle);
			return ret;
		}
		ret = osdlp_tc_get_rx_config(&tc, vcid);
		if (ret) {
			osMutexRelease(osdlp_tx_mtxHandle);
			return ret;
		}
		/* Copy length of TM in first two bytes */
		tm_proxy[0] = (length >> 8) & 0xff;
		tm_proxy[1] = length & 0xff;

		memcpy(&tm_proxy[2], pkt, length * sizeof(uint8_t));
		osdlp_prepare_clcw(tc, tm->ocf);
		register_meta(meta);
		ret = osdlp_tm_transmit(tm, tm_proxy, length + sizeof(uint16_t));
		osMutexRelease(osdlp_tx_mtxHandle);
		return ret;
	} else {
		return -OSDLP_MTX_LOCK;
	}
}
#endif /* if OSDLP_RX_ENABLE || OSDLP_TX_ENABLE*/

size_t
telemetry_basic_frame(const struct pq9ish *q, uint8_t *buffer)
{
	size_t cntr = 0;

	buffer[cntr++] = (q->uptime_secs >> 24) & 0xff;
	buffer[cntr++] = (q->uptime_secs >> 16) & 0xff;
	buffer[cntr++] = (q->uptime_secs >> 8) & 0xff;
	buffer[cntr++] = (q->uptime_secs) & 0xff;
	buffer[cntr++] = (q->reason_of_reset) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames_invalid >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames_invalid) & 0xff;
	buffer[cntr++] = (q->settings.reset_counters.low_power_counter >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.low_power_counter) & 0xff;
	buffer[cntr++] = (q->settings.reset_counters.independent_watchdog_counter >> 8)
	                 &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.independent_watchdog_counter) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.software_counter >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.software_counter) & 0xff;
	buffer[cntr++] = (q->settings.reset_counters.brownout_counter >> 8) &
	                 0xff;
	buffer[cntr++] = (q->settings.reset_counters.brownout_counter) & 0xff;
	buffer[cntr++] = (q->status.power_save) & 0x01;
	buffer[cntr++] = (q->status.power_mon_fail) & 0x01;

#if defined(OSDLP_RX_ENABLE) || defined(OSDLP_TX_ENABLE)
	struct tc_transfer_frame *tc;
	uint8_t clcw[4];
	int ret;
	for (int i = 0 ; i < OSDLP_TC_VCS ; i++) {
		ret = osdlp_tc_get_rx_config(&tc, i);
		if (ret) {
			buffer[cntr++] = 0xe0;
			buffer[cntr++] = 0xe0;
			buffer[cntr++] = 0xe0;
			buffer[cntr++] = 0xe0;
		} else {
			osdlp_prepare_clcw(tc, clcw);
			buffer[cntr++] = clcw[0];
			buffer[cntr++] = clcw[1];
			buffer[cntr++] = clcw[2];
			buffer[cntr++] = clcw[3];
		}
	}
#endif /* if OSDLP_RX_ENABLE || OSDLP_TX_ENABLE*/
	buffer[cntr++] = (q->hradio.tx_frames >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.tx_frames) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.rx_frames) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits >> 24) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits >> 16) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.rx_corrected_bits) & 0xff;
	buffer[cntr++] = ((int)q->hradio.hax5043.rx_status.rssi) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.freq_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.freq_tracking) & 0xff;
	buffer[cntr++] = ((int)q->hradio.hax5043.rx_status.agc) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking >> 24) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking >> 16) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.datarate_tracking) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.phase_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.phase_tracking) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking >> 24) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking >> 16) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking >> 8) & 0xff;
	buffer[cntr++] = (q->hradio.hax5043.rx_status.rf_freq_tracking) & 0xff;

	buffer[cntr++] = 0x4c;
	buffer[cntr++] = 0x69;
	buffer[cntr++] = 0x62;
	buffer[cntr++] = 0x72;
	buffer[cntr++] = 0x65;
	buffer[cntr++] = 0x53;
	buffer[cntr++] = 0x70;
	buffer[cntr++] = 0x61;
	buffer[cntr++] = 0x63;
	buffer[cntr++] = 0x65;
	return cntr;

}

int
retrieve_meta(struct pq9ish *q, struct tx_frame_metadata *meta)
{
	if (!meta) {
		return -1;
	}
	meta->baud = q->settings.tm_resp_baud;
	meta->enc = q->settings.tm_resp_enc;
	meta->mod = q->settings.tm_resp_mod;
	meta->tx_power = q->settings.tm_resp_tx_power;
	meta->enable_pa = 1;
	return 0;
}


void
telemetry_tx_callback(struct pq9ish *q)
{

	freq_mode_t fmode = FREQA_MODE;
	uint32_t freq = 435240000;
	uint32_t baud = 9600;
	uint32_t nframes = 1;
	uint32_t delay_ms = 1000;


	/*
	 * Periodical telemetry to be sent when the telemetry timer callback
	 * is called
	 * Prepare tmp_frame meta (tx configurations) and pdu (data) and send
	 * If using osdlp send using the transmit_tm function
	 */

//	struct tx_params p = {
//		.mod = FSK,
//		.baudrate = 9600,
//		.bandwidth = 2 * 9600,
//		.rf_out_mode = TXSE,
//		.shaping = UNSHAPED,
//		.pout_dBm = -2,
//		.fsk = {
//			.mod_index = 1.0,
//			.order = 1,
//			.freq_shaping = GAUSIAN_BT_0_5
//		},
//		.framing = RAW_PATTERN_MATCH,
//		.pattern = {
//			.preamble = 0x3333,
//			.preamble_len = 64,
//			.preamble_unencoded = 1,
//			.sync = 0x1ACFFC1D,
//			.sync_len = 32,
//			.sync_unencoded = 1,
//			.enc = {
//				.inv = 0,
//				.diff = 0,
//				.scrambler = 0,
//				.manch = 0,
//				.nosync = 0
//			},
//			.crc = {
//				.mode = CRC_OFF,
//				.init = 0xFFFFFFFF
//			},
//			.pkt = {
//				.addr_pos = 0,
//				.fec_sync_dis = 1,
//				.crc_skip_first = 1,
//				.msb_first = 1,
//				.len_pos = 0,
//				.len_bits = 8,
//				.len_offset = 0,
//				.max_len = 255,
//				.addr = 0,
//				.addr_mask = 0xFF
//			}
//		}
//	};
//	int ret = ax5043_conf_tx(&q->hradio.hax5043, &p);
//	if (ret) {
//		Error_Handler();
//	}
//	memset(tx.pdu, 0xB5, 512);
//	for (uint32_t i = 0; i < nframes; i++) {
//		tx.len = 120;
//		tx.timeout_ms = 5000;
//		while (xQueueSend(tx_queue, &tx, pdMS_TO_TICKS(1000)) != pdPASS) {
//			watchdog_reset_subsystem(&hwdg, wdid);
//		}
//		watchdog_reset_subsystem(&hwdg, wdid);
//		osDelay(delay_ms);
//	}
	tmp_frame.meta.mod = RADIO_MOD_FSK;
	tmp_frame.meta.enc = RADIO_ENC_RAW;
	tmp_frame.meta.baud = RADIO_BAUD_9600;

	tmp_frame.meta.tx_power = -2;
	tmp_frame.meta.enable_pa = 0;

	size_t len = 0;
	len = telemetry_basic_frame(q, tmp_frame.pdu);

	tmp_frame.len = len;
	tmp_frame.timeout_ms = 1000;

	extern QueueHandle_t tx_queue;
	xQueueSend(tx_queue, &tmp_frame, pdMS_TO_TICKS(5000));
	return;


#if defined(OSDLP_RX_ENABLE) || defined(OSDLP_TX_ENABLE)
	transmit_tm(tmp_frame.pdu, len, VCID_REG_TM, &tmp_frame.meta);
#endif /* if OSDLP_RX_ENABLE || OSDLP_TX_ENABLE*/
	return;
}
