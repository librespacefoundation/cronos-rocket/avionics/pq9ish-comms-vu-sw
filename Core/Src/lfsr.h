/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SRC_LFSR_H_
#define SRC_LFSR_H_

#include <stdint.h>

struct lfsr {
	uint32_t sr;
	uint32_t mask;
	uint8_t len;
};

void
lfsr_init(struct lfsr *h, uint32_t mask, uint32_t seed, uint8_t len);

uint8_t
lfsr_shift(struct lfsr *h);


#endif /* SRC_LFSR_H_ */
